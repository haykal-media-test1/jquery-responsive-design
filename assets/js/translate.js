const ar = {
    subscriptions: {
        title: 'الاشتراكات',
        digital: {
            title: 'الاشتراكات الرقمية',
            subscribe: 'اشترك الآن'
        },
        seemore: 'عرض المزيد'
    },
    footer: {
        aboutUs: 'معلومات حولنا',
        policies: 'سياسات الاشتراك',
        support: 'الدعم الفني',
        copyrights: 'جميع الحقوق محفوظة لشركة هارفارد بزنس ريفيو',
        contactUs: 'تواصل معنا',
        download: 'حمل تطبيق هارفارد بزنس ريفيو',
    }
};

function translateDocument() {

    const translates = $('.translate');
    Array.from(translates)
        .forEach(t => {

            const item = $(t);
            const data = item.data('name');

            let value = ar;
            data.split('.').forEach(e => value = value[e]);
            item.text(value);
        });
}

$(document).ready(translateDocument);